# Revision history for hasched

## 1.5.0.0 -- 2020.07.23
* Option to loop in a given interval, outputing only new events.
## 1.4.0.1 -- 2020.02.19
* Updated for time 1.9
## 1.4.0.0 -- 2019.05.30
* Parsing of single line entries.
* Parsing of week days (with hard-coded list of names).
## 1.3.0.0 -- 2019.05.24
* Added data structures for named week days. Parsing is not yet finished.
## 1.2.1.1 -- 2018.10.05
* Fixed a bug where time information would be incorrectly parsed after a list.
## 1.2.1.0 -- 2018.09.20
* Sorted output.
## 1.2.0.0 -- 2018.09.13
* Show multiple occurrences of whole day events if they happen on different days.
* Unspecified time information is interpreted as happening only once.
## 1.1.1.0 -- 2018.09.10
* Fixed a bug where the program would crash.
## 1.1.0.0 -- 2018.09.06
* Hasched now show several occurrences of the same event if they happen within the specified range. This can cause the program to produce infinite output if some event happens infinitely often and no range is specified.
## 1.0.0.0 -- 2018.09.03
* Added the ability of defining a list of days. months, etc.
* Improved printing format.
* Properly compute hour of events that happen in different days.
## 0.2.0.0 -- 2018.08.22
* Fixed a bug where periodic events which happened every hour would skip one hour.
* Fixed a bug where local time was being compared to UTC.
* Added proper documentation.
* Added option for summary, with each event appearing in a single line.
