NAME=hasched
Version=1.5.0.0
PKG=${NAME}-${Version}.tar.gz
SRC=$(wildcard src/*.hs) $(wildcard src/Hasched/*.hs)
BIN=dist/build/${NAME}/${NAME}
TOOLS=$(wildcard tools/*)
MANPAGE=${NAME}.1
Docs=${MANPAGE} README.md

all: ${PKG} ${Docs}

check: $(BIN)
	cd tests ; make -B Program=$(shell pwd)/$(BIN)

build: .cabal-sandbox ${SRC}
	cabal configure --disable-executable-dynamic
	cabal build

$(BIN): $(SRC)
	cabal build

${PKG}: build
	mkdir -p pkg/usr/bin
	mkdir -p pkg/usr/share/man
	cp ${BIN} ${TOOLS} pkg/usr/bin
	cp ${MANPAGE} pkg/usr/share/man/man1/
	tar -C pkg -caf ${PKG} usr

README.md: docs/README.body docs/README.header
	cd docs ; make README.md
	cp docs/README.md .

${MANPAGE}: docs/man.header docs/README.body
	cd docs ; make ${MANPAGE}
	cp docs/${MANPAGE} .

.cabal-sandbox:
	cabal sandbox init
	cabal install --only-dependencies
