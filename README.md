## Compiling

Install `ghc` and `cabal`. Then run

    cabal update
    cabal sandbox init
    cabal install --dependencies-only
    cabal build

This will generate the binary `hasched` in `dist/build/hasched/`.

# Synopsis

    hasched [OPTIONS...] FILE

# Description

A simple schedule manager for the command line and scripts. The idea is to use simple formats to allow more control for the user.

# Features

  - Periodic events (every year, month, day, hour, minute or second).
  - Events have a title and optional description.
  - Accumulate past deadlines into a file (for todos).

# Options

  **`-d, --distance TIME`**

  Shows events happening within TIME time. TIME is in the format AwBdChDmEs, where A, B, C, D, E are integers and w, d, h, m, s stand for weeks, days, hours, minutes and seconds.

  **`-e, --escape`**

  Escape all newlines and tabs so that the output fits into one line (for scripts).

  **`-h, --help`**

  Show help.

  **`-s, --summary`**

  Shows one event per line, together with occurrence time.

  **`-t, --todo FILE`**

  Accumulates all events of FILE into to the output. The file acts as a todo list, in the sense that events will accumulate after their deadline unless explicitly removed. The output will only contain fixed dates.    

  **`-v, --version`**

  Output version and exit

# File Format

Files passed to hasched should be in the simtreelo format, which describes data as a tree. The first line of the file specifies the pattern for comments and can be left blank to disable comments. Children of a node are in one indentation level higher than the parent, and the indentation level is given by the first line that begins with spaces or tabs. Indentation must be consistent across the whole file.

Events have three parts: Time, title and description. Time is specified in the format Y.m.d.h.M.s, although '.' can be replaced by any of '-_:.,/\ '. Children of a node inherit time information from the parents, and time information can also be partially specified. Once node that does not encode time is found, its text is treated as the title of the event, and the children are concatenated and interpreted as its description.
Each value for time is either a number, a wildcard '*' or a list of numbers around brackets separated by commas, as in '(3, 4, 12)'.

# Examples

To describe Einstein's birthday and Pi day one could write in the file `events`:

    #
    *
        3.14
            Einstein's birthday # This is a comment
                Don't forget the cake!
            Pi day
                pi = 3.141...

and then call `hasched --distance 30d events` to know if any of the events happen within 30 days.

To mark all prime days one can write in the file `prime`:

    #
    *.*.(2,3,5,7,11,13,17,19,23,29,31)
        Prime Day

and then call `hasched --distance 1w prime` to know if any of the next 7 days are prime numbered.

# Bugs

No known bugs.

# Author

Marcelo Garlet Millani (marcelogmillani@gmail.com)
