--  Copyright 2016 Marcelo Garlet Millani

--  This file is part of hasched.

--  hasched is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  hasched is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with hasched.  If not, see <http://www.gnu.org/licenses/>.

module Main where

import Control.Concurrent
import Control.Monad
import Control.Exception
import Data.Char
import Data.Either
import Data.List
import Data.Maybe
import Data.Time
import Data.Tree
import System.Directory
import System.Environment
import System.FilePath
import System.IO
import System.IO.Error

import qualified Data.Set       as S
import qualified Data.Simtreelo as Simtreelo

import Hasched.Entities
import Hasched.Time
import qualified Hasched.Loader as L


import qualified Debug.Trace as D

newtype EventHistory = EH (String, LocalTime)

instance Eq EventHistory where
  (EH (s1, t1)) == (EH (s2, t2))
    | t1 == t2 = s1 == s2
    | otherwise = False
instance Ord EventHistory where
  compare (EH (s1, t1)) (EH (s2, t2))
    | t1 == t2 = compare s1 s2
    | otherwise = compare t1 t2
instance Show EventHistory where
  show (EH (s, _)) = s

appName = "hasched"
appVersion = "1.5.0.0"
appDate = "2020.07.23"

versionText = concat $ [appName, " ", appVersion, " (", appDate, "), released under GPLv3"]

helpText =
  [ versionText
  , ""
  , "usage:"
  , appName ++ " [OPTION...] <FILE...>", ""
  , "where OPTION is one of:"
  , "  -d, --distance TIME   Shows events happening within TIME time. TIME is in the format AwBdChDmEs,"
  , "                      where A, B, C, D, E are integers and w, d, h, m, s stand for weeks, days, hours,"
  , "                      minutes and seconds."
  , "  -e, --escape          Escape all newlines and tabs so that the output fits into one line."
  , "  -h, --help            This help."
  , "  -l, --loop TIME       Repeatedly check for new events, waiting TIME between each check (see above for the format of TIME)."
  , "  -s, --summary         Shows one event per line, together with occurrence time."
  , "  -t, --todo FILE       Accumulates all events of FILE into to the output."
  , "                        The file acts as a todo list, in the sense that events"
  , "                        will accumulate after their deadline unless explicitly removed."
  , "                        The output will only contain fixed dates."
  , "  -v, --version         Version."  
  ]

-- | gets the directory where configuration files should be placed
--
-- First, checks if XDG_CONFIG_HOME exists, producing $XDG_CONFIG_HOME/appName if it does
-- if it does not, the checks if HOME does, producing $HOME/.config/appName if it does
-- if it still fails, returns getAppUserDataDirectory appName
getConfigDirectory appName =
  let failXDG e = do
        dir <- getEnv "HOME"
        return $ dir ++ [pathSeparator] ++ ".config" ++ [pathSeparator] ++ appName
      failHOME e = getAppUserDataDirectory appName
  in
   do
     handle (failHOME::SomeException -> IO FilePath) $
       handle (failXDG::SomeException -> IO FilePath) $ do
         dir <- getEnv "XDG_CONFIG_HOME"
         return $ dir ++ [pathSeparator] ++ appName

data Action =
  Action
  { help      :: Bool
  , version   :: Bool
  , files     :: [String]
  , distance  :: NominalDiffTime
  , escapeOut :: Bool
  , todoFile  :: Maybe String
  , outputF   :: (TimeEvent, LocalTime) -> String
  , loopInterval :: Maybe Integer
  }

parseArgs action args = case args of
  "-h":r     -> parseArgs (action{help = True}) r
  "--help":r -> parseArgs (action{help = True}) r
  "-v":r        -> parseArgs (action{version = True}) r
  "--version":r -> parseArgs (action{version = True}) r
  "-d":d:r         -> parseArgs (action{distance = fromIntegral $ timeDistance d}) r
  "--distance":d:r -> parseArgs (action{distance = fromIntegral $ timeDistance d}) r
  "-e":r        -> parseArgs (action{escapeOut = True}) r
  "--escape":r  -> parseArgs (action{escapeOut = True}) r
  "-s":r        -> parseArgs (action{outputF = showSummary}) r
  "--summary":r -> parseArgs (action{outputF = showSummary}) r
  "-t":t:r     -> parseArgs (action{todoFile = Just t}) r
  "--todo":t:r -> parseArgs (action{todoFile = Just t}) r
  "-l":d:r     -> parseArgs (action{loopInterval = Just $ fromIntegral $ timeDistance d}) r
  "--loop":d:r -> parseArgs (action{loopInterval = Just $ fromIntegral $ timeDistance d}) r
  h:r -> parseArgs (action{files = h : files action}) r
  [] -> action
  where
    timeDistance [] = 0
    timeDistance str =
      let (n', rs) = span isNumber str
          n = read n' :: Int
          (t, rs') = span isAlpha rs
          m = timeDistance rs'
      in m + case toLower $ head t of
              'w' -> n * week
              'd' -> n * day
              'h' -> n * hour
              'm' -> n * minute
              's' -> n
    minute = 60
    hour = 60 * minute
    day = 24 * hour
    week = 7 * day

defaultAction = Action
  { help = False
  , files = []
  , distance = 0
  , escapeOut = False
  , todoFile = Nothing
  , outputF = outputString
  , version = False
  , loopInterval = Nothing
  }

delay time
  | time <= 0 = return ()
  | otherwise = do
    let maxWait = min time $ toInteger (maxBound :: Int)
    threadDelay $ fromInteger maxWait
    when (maxWait /= time) $ delay (time - maxWait)

escape ('\n':xs) = '\\':'n':escape xs
escape ('\t':xs) = '\\':'t':escape xs
escape ('\\':xs) = '\\':'\\':escape xs
escape (x:xs) = x:escape xs
escape [] = []

execute action today printed
  | help action = mapM_ putStrLn helpText
  | version action = putStrLn versionText
  | files action /= [] = do
    todo <- case todoFile action of
      Just fl -> catch (Simtreelo.loadFile fl) $ \e -> if isPermissionError e then return $ Left $ "You do not have the required permissions to open " ++ fl else return $ Right []
      Nothing -> return $ Right []
    events <- mapM L.loadFile $ files action
    let eventFilter = if (distance action) == 0 then from today else within (distance action) today
        (errors, ok) = partitionEithers events
        events' = sortBy (\(_, t0) (_, t1) -> compare t0 t1) $ concatMap (getEvents eventFilter today) ok :: [(TimeEvent, LocalTime)]
    mapM_ (hPutStrLn stderr) errors
    let outputF'
          | escapeOut action = escape . (outputF action)
          | otherwise = outputF action
        printed' = S.filter (from today . getEHTime) printed
        eventsStr = filter (not . (flip S.member) printed') $ map (\(e, t) -> EH ((outputF' (e,t)), t)) events'
        printed'' = S.union printed' (S.fromList eventsStr)
    mapM_ (putStr . show) eventsStr
    case loopInterval action of
      Just d -> do
        hFlush stdout -- useful when piping output
        delay (d * 1000000)
        today <- nowInUtc
        execute action today printed''
      Nothing -> return ()
  | otherwise = mapM_ putStrLn helpText

getEvents p today events = [(e, t) | e <- events, t <- takeWhile p $ allOccurrences e today]

getEHTime (EH (_,t)) = t

within dst today time =
  let d = diffLocalTime time today
  in d > -1 && d <= dst -- -1 because we work on seconds, but LocalTime is actually more precise

from today time =
  let d = diffLocalTime time today
  in d > -1 -- -1 because we work on seconds, but LocalTime is actually more precise

showSummary evs = 
  showEvent evs
  where
    showEvent (e, te) = 
      showGregorian (localDay te) ++ ' ':(stringTime te) ++ " " ++ description e ++ "\n"

outputString evs =
  Simtreelo.toString [timeEventToTreeNode evs] "\t"  

nowInUtc = do
  ctime <- getCurrentTime
  timezone <- getCurrentTimeZone
  return $ utcToLocalTime timezone ctime

main = do
  args <- getArgs
  let action = parseArgs defaultAction args
  today <- nowInUtc
  execute action today S.empty
