--  Copyright 2016 Marcelo Garlet Millani
--  This file is part of Hasched.

--  Hasched is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  Hasched is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with Hasched.  If not, see <http://www.gnu.org/licenses/>.

module Hasched.Entities where

import Data.List
import qualified Data.Tree as T
import Data.Time.Clock
import Data.Time
import Data.Time.LocalTime
import Data.Time.Calendar.WeekDate
import Hasched.Time

data TimeInfo a = Day DayOfWeek | Number a deriving (Eq, Show, Ord)
data Periodic a = Never | Once | Always | Only [a] | Range a a deriving (Show, Read, Eq, Ord)

isNever Never = True
isNever _ = False

isAlways Always = True
isAlways _ = False

isOnly (Only _) = True
isOnly _ = False

isRange (Range _ _) = True
isRange _ = False

combinePeriodic Never t1 = t1
combinePeriodic t0 _ = t0

toRange mn mx Always = Range (Number mn) (Number mx)
toRange _ _ x = x

inPeriod t Always = True
inPeriod t (Only x) = t `elem` x
inPeriod t (Range mn mx) = t >= mn && t <= mx
inPeriod t Never = False

earliestFrom t Always   = Just t
earliestFrom t Once     = Just t
earliestFrom t (Only x) =
  let ts = dropWhile (< t) x
  in if null ts then Nothing else Just $ head ts
earliestFrom t (Range mn mx) = if inPeriod t (Range mn mx) then Just t else Nothing


allAfter t0 Always = [t0..]
allAfter t0 Once = [t0]
allAfter t0 (Range mn mx) = [t0..mx]
allAfter t0 (Only  x) = dropWhile (< t0) x
allAfter _  Never = []

repeatPeriodic t0  Always = [t0..]
repeatPeriodic t0  Once   = [t0]
repeatPeriodic t0 (Range mn mx) =
  let t0' = max mn t0
      t1 = pred t0
  in [t0'..mx] ++ if t1 > mn then [mn..pred t0] else []
repeatPeriodic t0 (Only  x) =
  let (less, greater) = span (<t0) x in greater ++ less
repeatPeriodic _ Never = []

toNumberedTime year' month' periodic =
  let year = fromIntegral year'
      month = fromIntegral month'
      d1 = dayOfWeek $ fromGregorian year month 1
  in case periodic of
    Never -> Never
    Always -> Always
    Once -> Once
    Range mn mx ->
      case (mn, mx) of
        (Number n, Number x) -> Range n x
        (Day n, Day x) -> 
          let dn = 1 + ((fromEnum n) - (fromEnum d1)) `mod` 7
              dx = dn + ((fromEnum x) - (fromEnum n)) `mod` 7
          in Only $ [ fromIntegral $ w + day
                    | w <- takeWhile (\x -> x + dx <= (gregorianMonthLength year month)) [0,7..28]
                    , day <- [dn..dx]]
    Only list -> Only $ sort $ concatMap (\x -> case x of
        Day d -> let d0 = 1 + ((fromEnum d) - (fromEnum d1)) `mod` 7
                 in map fromIntegral $ [d0,d0+7..gregorianMonthLength year month]
        Number x -> [x]) list

data TimeEvent =
  TimeEvent {
      -- | simple description of what kind of event is this (exam, pizza ...)
      description :: String
      -- | generic information about the event (exam subject, pizza flavour ...)
    , information :: [String]
      -- | given a point in time, returns a list of all occurrences of the event from that point on
    , allOccurrences :: LocalTime -> [LocalTime]
    }

-- | Returns 't0' if ´t1' comes before it, otherwise returns 'Nothing'.
-- | To be used in a 'TimeEvent' for single occurrence events
singleOccurrence t0 t1
  | diffLocalTime t0 t1 < 0 = Nothing
  | otherwise = Just t0

-- This function is available only in a newer version of time
addLocalTime x = utcToLocalTime utc . addUTCTime x . localTimeToUTC utc

timeEventToTreeNode (e, te) =
  T.Node
  { T.rootLabel = showGregorian (localDay te) ++ ' ':(stringTime te)
  , T.subForest = [T.Node
    { T.rootLabel = description e
    , T.subForest = map (\x -> T.Node{T.rootLabel = x, T.subForest = []} ) $ information e
    }]
  }

diffTimeEvent te t0 = fmap (\x -> diffLocalTime x t0) te

instance Eq TimeEvent where
  (TimeEvent{description = d0, information = i0}) == (TimeEvent{description = d1, information = i1}) =
    d1 == d0 && i0 == i1

instance Show TimeEvent where
  show tev =
    (description tev) ++ "\n" ++
    (concatMap ('\t':) $ information tev)

data Activity a = Activity Day (a, a) [String] deriving (Eq,Show)

instance Ord a => Ord (Activity a) where
  compare (Activity d1 r1 desc1) (Activity d2 r2 desc2)
    | d1 == d2 && r1 == r2 =  compare desc1 desc2
    | d1 == d2 = compare r1 r2
    | otherwise = compare d1 d2

activityToTikz (Activity d (s,e) info) =
  "\\draw[fill = red]" ++ start ++ "rectangle" ++ end ++ ";\n" ++
  infoTex
  where
    (_,_, weekd) = toWeekDate d
    wtex = ["\\mon", "\\tue", "\\wed", "\\thu", "\\fri", "\\sat", "\\sun"] !! (weekd-1)
    start = "(" ++ wtex ++ "+\\actstart," ++ (show s) ++ ")"
    end   = "(" ++ wtex ++ "+\\actend,"   ++ (show e) ++ ")"
    infoTex = concat $ map infoToTex $ zip info [1..]
    infopos = wtex ++ " + \\actend/2 + \\actstart/2"
    infoToTex (t,i) = "\\node () at (" ++ infopos ++ "," ++ (show $ s + (15/60)*(fromIntegral i)) ++ ") {\\tiny " ++ t ++ "};\n"
