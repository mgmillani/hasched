--  Copyright 2016 Marcelo Garlet Millani
--  This file is part of Hasched.

--  Hasched is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  Hasched is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with Hasched.  If not, see <http://www.gnu.org/licenses/>.


module Hasched.Time where

import Data.Time
import Data.Time.LocalTime

-- instance Ord DayOfWeek where
--   compare a b  = compare (fromEnum a) (fromEnum b)

unpackTime t1 =
  let sect1 = (diffTimeToPicoseconds $ timeOfDayToTime $ localTimeOfDay t1) `div` (10^12)
  in (sect1 `div` 3600, sect1 `div` 60 `mod` 60, sect1 `mod` 60)

stringTime t1 =
  let (h,m,s) = unpackTime t1
      showLZ x = if x < 10 then '0' : show x else show x
  in concat $ [showLZ h, ":", showLZ m]  ++ if s > 0 then [":" ,showLZ s] else ["   "]
