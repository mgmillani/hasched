--  Copyright 2016 Marcelo Garlet Millani
--  This file is part of Hasched.

--  Hasched is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  Hasched is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with Hasched.  If not, see <http://www.gnu.org/licenses/>.


module Hasched.Loader where

import Data.Maybe
import Data.Time
import Data.Time.LocalTime
import Data.Tree
import qualified Data.Simtreelo as Simtreelo
import Hasched.Entities
import Hasched.Time
import Control.Monad.State
import Data.Char
import Data.List
import qualified Data.Map as M
import Text.Read (readEither)

import qualified Debug.Trace as D

data Timestamp a =
  Timestamp {
      year   :: Periodic a
    , month  :: Periodic a
    , day    :: Periodic a
    , hour   :: Periodic a
    , minute :: Periodic a
    , second :: Periodic a
    } deriving (Show, Read, Eq)

noTime = Timestamp{year = Never, month = Never, day = Never, hour = Never, minute = Never, second = Never}
everytime = Timestamp{year = Always, month = Always, day = Always, hour = Always, minute = Always, second = Always}
onceUponATime = Timestamp{year = Once, month = Once, day = Once, hour = Once, minute = Once, second = Once}

defaultWeekDays = M.fromList $ zip ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"] [Monday .. Sunday]

combineTimestamps t1 t2 =
  t1{ year = combinePeriodic (year t1) (year t2)
    , month = combinePeriodic (month t1) (month t2)    
    , day = combinePeriodic (day t1) (day t2)
    , hour = combinePeriodic (hour t1) (hour t2)
    , minute = combinePeriodic (minute t1) (minute t2)
    , second = combinePeriodic (second t1) (second t2)}

rangedTimestamp t1 =
  t1{ month  = toRange 1 12 $ month t1
    , day    = toRange 1 31 $ day t1
    , hour   = toRange 0 23 $ hour t1
    , minute = toRange 0 59 $ minute t1
    , second = toRange 0 59 $ second t1
    }

asList t =
  [ year t
  , month t
  , day t
  , hour t
  , minute t
  , second t
  ]

splitOn p s =
  case dropWhile p s of
    [] -> []
    s' -> w : splitOn p s''
      where
        (w, s'') = break p s'

consumeWhile :: (Char -> Bool) -> State String String
consumeWhile f = do
  str <- get
  let (x,r) = span f str
  put $ r
  return x

timeSeparators = "-_:.,/\\ \t"

parseTimestamp weekDays = do
  consumeWhile (`elem` timeSeparators)
  str <- get
  case str of
    [] -> return []
    '(':rs -> do
      put rs
      lst <- parseList weekDays
      ts <- parseTimestamp weekDays
      return $ (Only $ lst) : ts
    '*':rs -> do
      put rs
      consumeWhile (`elem` timeSeparators)
      ts <- parseTimestamp weekDays
      return (Always:ts)
    a:rs -> if isNumber a
            then do
              t <- consumeWhile isNumber
              ts <- parseTimestamp weekDays
              return ((Only $ [Number $ read t]):ts)
            else do -- check if it is the name of a week day
              return []

parseList weekDays = do
  lst <- consumeWhile (/=')')
  consumeWhile (==')')
  return $ 
    map (\e -> case readEither e of
      Right n -> Number n
      Left _ -> case (map toLower e) `M.lookup` weekDays of
                  Just d -> Day $ d
                  Nothing -> Number (-1))
      $ splitOn (`elem` " ,") lst

parseLine weekDays = do
  str <- get
  ts <- parseTimestamp weekDays
  consumeWhile (`elem` timeSeparators)
  rs <- get
  return $ (ts,rs)

updateTimestamp t tlist = Timestamp{year = y, month = mo, day = d, hour = h, minute = mi, second = s}
  where
    -- parts = evalState (parseTimestamp defaultWeekDays) str
    [y,mo,d,h,mi,s] = fillTimestamp tlist $ asList t
    fillTimestamp (x:xs) (Never:ts) = x : fillTimestamp xs ts
    fillTimestamp xs  ((Only t):ts) = (Only $ sort t) : fillTimestamp xs ts
    fillTimestamp xs     (t:ts) = t : fillTimestamp xs ts
    fillTimestamp []     ts  = ts
    fillTimestamp _      []  = []

loadFile :: FilePath -> IO (Either [Char] [TimeEvent])
loadFile file = do
  tree <- Simtreelo.loadFile file
  return (fmap (concatMap parse) tree)

loadString :: String -> Either [Char] [TimeEvent]
loadString string =
  let tree = Simtreelo.loadString string in fmap (concatMap parse) tree

-- isValidTime = all (\x -> x `elem` ("0123456789*()" ++ timeSeparators))

isPeriodic t = not $ null $ filter isAlways (asList t)

allFrom ts t1 =
  [toLocalTime y mo d h mi s
  | (y, mo, d) <- validDays
  , (h, mi, s) <- if (y, mo, d) == (y1, mo1, d1) then todayHours else futureHours]
  where
    (ys':mos':ds':hs':mins':ss':[]) = asList ts
    [mos,hs,mins,ss] = map (toNumberedTime 2019 5) [mos',hs',mins',ss'] -- Year and month do not make a different since we are not converting days.
    ys = toNumberedTime 2019 5 ys'
    -- current time
    (y1, mo1int, d1int)  = toGregorian $ localDay t1
    mo1 = fromIntegral mo1int
    d1 = fromIntegral d1int
    (h1, min1, s1) = unpackTime t1
    l1 = [y1, mo1, d1, h1, min1, s1]
    -- resulting time
    validDays = 
      [ (y,m,d) |
          y <- allAfter y1 ys
        , m <- if y == y1             then allAfter mo1 mos else allAfter 1 mos
        , let ds = toNumberedTime y m ds' 
        , d <- if y == y1 && m == mo1 then allAfter d1  ds  else allAfter 1 ds
        , (isJust $ fromGregorianValid y (fromIntegral m) (fromIntegral d))
      ]
    todayHours =
      [ (h,m,s) |
           h <- allAfter h1 hs
         , m <- if h == h1              then allAfter min1 mins else allAfter 0 mins
         , s <- if h == h1 && m == min1 then allAfter s1   ss   else allAfter 0 ss
      ]
    futureHours =
      [ (h,m,s) |
          h <- allAfter 0 hs
        , m <- allAfter 0 mins
        , s <- allAfter 0 ss
      ]
    toLocalTime y mo d h mi s = LocalTime{localDay = fromGregorian y (fromIntegral mo) (fromIntegral d), localTimeOfDay = TimeOfDay{todHour = fromIntegral h, todMin = fromIntegral mi, todSec = fromIntegral s}}

parse rawTree = parse' noTime rawTree

parse' t Node{rootLabel = label, subForest = children} = 
  let (ts, desc) = evalState (parseLine defaultWeekDays) label
  in case desc of
      [] -> concatMap (parse' $ updateTimestamp t ts) children
      _ -> 
        let info = map (flattenTree 0) children
            t' = rangedTimestamp $ combineTimestamps (updateTimestamp t ts) onceUponATime
        in [TimeEvent{description = desc
                     , information = info
                     , allOccurrences = allFrom t'}]
--  | isValidTime label =
--    let t' = (updateTimestamp t label) in
--      concatMap (parse' t') children
--  | otherwise =

flattenTree i Node{rootLabel = lb, subForest = fs} = (take i $ repeat '\t') ++ lb ++ if null fs then [] else ('\n' : (concatMap (flattenTree (i+1)) fs) )
