{ mkDerivation, base, containers, directory, filepath, lib, mtl
, simtreelo, time
}:
mkDerivation {
  pname = "hasched";
  version = "1.5.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base containers directory filepath mtl simtreelo time
  ];
  description = "A scheduler for appointments";
  license = lib.licenses.gpl3Only;
}
